# React Native Conekta WKWebView

**_React Native Conekta SDK for iOS and Android_**

## Installation

`npm install --save react-native-conekta-wkwebview` or

`yarn add react-native-conekta-wkwebview`

`react-native link`

`cd ios`

`pod install`

## Usage

```javascript
var conektaApi = new Conekta();

conektaApi.setPublicKey("YOUR_PUBLIC_KEY");

conektaApi.createToken(
  {
    cardNumber: "4242424242424242",
    name: "Manolo Virolo",
    cvc: "111",
    expMonth: "11",
    expYear: "21",
  },
  function (data) {
    console.log("DATA:", data); // data.id to get the Token ID
  },
  function () {
    console.log("Error!");
  }
);
```

## iOS Manual Installation (if you did not use `react-native link`)

[Please see: Linking Libraries iOS](https://facebook.github.io/react-native/docs/linking-libraries-ios.html#content)

Library folder: `your-project/node_modules/react-native-conekta-wkwebview-wkwebview/RNConekta`

## Android Manual Installation (if you did not use `react-native link`)

In `android/settings.gradle`

```gradle
...

include ':react-native-conekta-wkwebview'
project(':react-native-conekta-wkwebview').projectDir = file('../node_modules/react-native-conekta-wkwebview/android')
```

In `android/app/build.gradle`

```gradle
...

dependencies {
    ...

    compile project(':react-native-conekta-wkwebview')
}
```

Manually register module in `MainApplication.java`:

```java
import com.dieam.reactnativeconekta.ReactNativeConektaPackage;  // <--- import

public class MainApplication extends Application implements ReactApplication {

  ......
  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
      @Override
      protected boolean getUseDeveloperSupport() {
        return BuildConfig.DEBUG;
      }

      @Override
      protected List<ReactPackage> getPackages() {

      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
          new ReactNativeConektaPackage() // <---- Add the Package
      );
    }
  };

  ....
}

```
