require 'json'

package = JSON.parse(File.read(File.join(__dir__, 'package.json')))

Pod::Spec.new do |s|
  s.name         = package['name']
  s.version      = package['version']
  s.summary      = package['description']
  s.license      = package['license']

  s.authors      = package['author']
  s.homepage     = package['homepage']
  s.platform     = :ios, "9.0"

  s.source       = { :git => 'https://gitlab.com/jorgeogj/react-native-conekta-wkwebview.git'}
  s.source_files  = "RNConekta/**/*.{h,m}"

  s.dependency 'React'
end
